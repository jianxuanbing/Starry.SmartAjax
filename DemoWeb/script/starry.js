﻿(function ($, starry, undefined) {
    if ($ === undefined) {
        throw "jQuery组件加载失败";
    }

    var _starry = starry || {};
    $.extend(_starry, {
        tips: $.extend(_starry.tips || {}, {
            onSuccess: function (message) { },
            onWarning: function (message) { },
            onError: function (message) { }
        }),
        smartAjax: function (options) {
            $.ajax(options.url || "ajax.ashx", $.extend({ type: "GET", dataType: "json", asycn: true, cache: false }, options))
                .done(options.success ? options.success : function (bizResult) {
                    if (bizResult) {
                        if (bizResult.IsSuccess) {
                            var lightSuccess = options.lightSuccess || function (bizResult) { };
                            lightSuccess(bizResult);
                        }
                        else {
                            var lightFailed = options.lightFailed || function (bizResult) {
                                alert("执行失败" + bizResult.Message);
                            };
                            lightFailed(bizResult);
                        }
                    }
                });
        }
    });

    window.starry = _starry;
})(jQuery, window.starry);