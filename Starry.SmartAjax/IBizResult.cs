﻿using System;
using System.Collections.Generic;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务执行结果接口
    /// </summary>
    public interface IBizResult
    {
        /// <summary>
        /// 获取或设置结果实体
        /// </summary>
        object Entity { get; set; }
        /// <summary>
        /// 获取或设置业务执行成功失败标识
        /// </summary>
        bool IsSuccess { get; set; }
        /// <summary>
        /// 获取消息记录集的第一条记录
        /// </summary>
        string Message { get; }
        /// <summary>
        /// 获取或设置消息记录集
        /// </summary>
        IList<string> Messages { get; set; }
        /// <summary>
        /// 将该结果对象反应到HTTP的响应流中
        /// </summary>
        /// <param name="context">HTTP上下文</param>
        void Response(System.Web.HttpContext context);
    }
}
