﻿using System;
namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务事务接口
    /// </summary>
    public interface IBizTranscation : ITransactable
    {
    }
}
