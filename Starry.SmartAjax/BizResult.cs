﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务执行结果
    /// </summary>
    [Serializable]
    public class BizResult : IBizResult
    {
        /// <summary>
        /// 创建一个业务执行结果对象，默认结果为成功
        /// </summary>
        public BizResult() : this(true) { }
        /// <summary>
        /// 用指定业务执行成功失败标识创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        public BizResult(bool isSuccess) : this(isSuccess, null as IList<string>) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及一条消息记录创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="message">一条消息记录</param>
        public BizResult(bool isSuccess, string message) : this(isSuccess, new string[] { message }) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及消息记录集创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="messages">消息记录集</param>
        public BizResult(bool isSuccess, IList<string> messages) : this(isSuccess, messages, new object()) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及消息记录集和结果实体创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="messages">消息记录集</param>
        /// <param name="entity">结果实体</param>
        public BizResult(bool isSuccess, IList<string> messages, object entity)
        {
            this.IsSuccess = isSuccess;
            this.Messages = messages ?? new List<string>();
            this.Entity = entity;
        }
        /// <summary>
        /// 获取或设置业务执行成功失败标识
        /// </summary>
        public bool IsSuccess { set; get; }
        /// <summary>
        /// 获取或设置消息记录集
        /// </summary>
        public IList<string> Messages { set; get; }
        /// <summary>
        /// 获取消息记录集的第一条记录，如果不存在则为空字符串
        /// </summary>
        public string Message { get { return this.Messages.FirstOrDefault() ?? string.Empty; } }
        /// <summary>
        /// 获取或设置结果实体
        /// </summary>
        public object Entity { set; get; }
        /// <summary>
        /// 将该结果对象反应到HTTP的响应流中
        /// </summary>
        /// <param name="context">HTTP上下文</param>
        public virtual void Response(System.Web.HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(new JavaScriptSerializer().Serialize(this));
        }
    }
    /// <summary>
    /// 指定结果实体类型的业务执行结果
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    [Serializable]
    public class BizResult<TEntity>
        : BizResult
    {
        /// <summary>
        /// 创建一个业务执行结果对象，默认结果为成功
        /// </summary>
        public BizResult() : this(true) { }
        /// <summary>
        /// 用指定业务执行成功失败标识创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        public BizResult(bool isSuccess) : this(isSuccess, null as IList<string>) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及一条消息记录创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="message">一条消息记录</param>
        public BizResult(bool isSuccess, string message) : this(isSuccess, new string[] { message }) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及消息记录集创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="messages">消息记录集</param>
        public BizResult(bool isSuccess, IList<string> messages) : this(isSuccess, messages, default(TEntity)) { }
        /// <summary>
        /// 用指定业务执行成功失败标识及消息记录集和结果实体创建一个业务执行结果对象
        /// </summary>
        /// <param name="isSuccess">业务执行成功失败标识</param>
        /// <param name="messages">消息记录集</param>
        /// <param name="entity">结果实体</param>
        public BizResult(bool isSuccess, IList<string> messages, TEntity entity) : base(isSuccess, messages, entity) { }
        /// <summary>
        /// 获取或设置结果实体
        /// </summary>
        new public TEntity Entity
        {
            set { base.Entity = value; }
            get { return (TEntity)base.Entity; }
        }
    }
}