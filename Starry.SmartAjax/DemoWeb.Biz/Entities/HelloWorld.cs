﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoWeb.Biz.Entities
{
    public class HelloWorld
        : DemoBizEntity
    {
        public override Starry.SmartAjax.BizResult Execute()
        {
            return new Starry.SmartAjax.BizResult { Entity = "Hello World !" };
        }
    }
}
