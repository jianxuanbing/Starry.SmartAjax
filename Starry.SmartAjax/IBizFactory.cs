﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务工厂接口
    /// </summary>
    public interface IBizFactory
    {
        /// <summary>
        /// 根据HTTP上下文的内容创建业务实体
        /// </summary>
        /// <param name="context">HTTP上下文</param>
        /// <returns>创建的业务实体</returns>
        IBizEntity CreateBizEntity(System.Web.HttpContext context);
    }
}
