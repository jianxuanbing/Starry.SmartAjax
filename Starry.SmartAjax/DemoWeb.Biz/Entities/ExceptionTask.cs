﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoWeb.Biz.Entities
{
    class ExceptionTask
        : DemoBizEntity
    {
        public override Starry.SmartAjax.BizResult Execute()
        {
            var a = 1;
            var b = 0;
            var c = a / b;
            return new Starry.SmartAjax.BizResult();
        }
    }
}
