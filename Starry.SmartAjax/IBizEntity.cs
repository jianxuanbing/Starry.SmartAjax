﻿using System;
namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务实体接口
    /// </summary>
    public interface IBizEntity
    {
        /// <summary>
        /// 执行业务并获取执行结果
        /// </summary>
        /// <returns>业务执行结果</returns>
        BizResult Execute();
        /// <summary>
        /// 获取或设置该业务执行所需的HTTP上下文
        /// </summary>
        System.Web.HttpContext HttpContext { set; get; }
        /// <summary>
        /// 获取HTTP上下文中requestName对应的请求参数
        /// </summary>
        /// <param name="requestName">请求参数名</param>
        /// <returns>所请求的参数</returns>
        string this[string requestName] { get; }
        /// <summary>
        /// 获取或设置业务执行关联的事务
        /// </summary>
        IBizTranscation BizTranscation { set; get; }
        /// <summary>
        /// 创建一个业务执行事务
        /// </summary>
        /// <returns>创建的业务执行事务</returns>
        IBizTranscation BeginBizTranscation();
    }
}
