﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务实体
    /// </summary>
    public abstract class BizEntity : IDisposable, IBizEntity
    {
        /// <summary>
        /// 创建业务实体
        /// </summary>
        protected BizEntity() { }
        /// <summary>
        /// 执行业务并获取执行结果
        /// </summary>
        /// <returns>业务执行结果</returns>
        public abstract BizResult Execute();
        /// <summary>
        /// 获取或设置该业务执行所需的HTTP上下文
        /// </summary>
        public HttpContext HttpContext { set; get; }
        /// <summary>
        /// 获取HTTP上下文中requestName对应的请求参数
        /// </summary>
        /// <param name="requestName">请求参数名</param>
        /// <returns>所请求的参数</returns>
        public virtual string this[string requestName]
        {
            get { return (this.HttpContext.Request[requestName] ?? this.HttpContext.Request.Form[requestName]) ?? string.Empty; }
        }
        /// <summary>
        /// 释放资源的方法
        /// </summary>
        public virtual void Dispose() { }
        /// <summary>
        /// 获取或设置业务执行关联的事务
        /// </summary>
        public IBizTranscation BizTranscation { set; get; }
        /// <summary>
        /// 创建一个业务执行事务
        /// </summary>
        /// <returns>创建的业务执行事务</returns>
        public virtual IBizTranscation BeginBizTranscation()
        {
            if (!(this is ITransactable))
            {
                throw new ArgumentException("该业务不支持事务", "bizEntity");
            }
            this.BizTranscation = new BizTranscation(this as ITransactable);
            return this.BizTranscation;
        }
    }
}