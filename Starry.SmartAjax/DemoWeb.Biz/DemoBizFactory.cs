﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoWeb.Biz
{
    class DemoBizFactory : Starry.SmartAjax.IBizFactory
    {
        public Starry.SmartAjax.IBizEntity CreateBizEntity(System.Web.HttpContext context)
        {
            var requestBizName = context.Request["BizName"] ?? string.Empty;
            var bizEntity = System.Reflection.Assembly.GetExecutingAssembly().CreateInstance("DemoWeb.Biz.Entities." + requestBizName) as Starry.SmartAjax.BizEntity;
            if (bizEntity == null)
            {
                throw new Exception(string.Format("业务创建失败，没有业务名为{0}的业务", requestBizName));
            }
            return bizEntity;
        }
    }
}
