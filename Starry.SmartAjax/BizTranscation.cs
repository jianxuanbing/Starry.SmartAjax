﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 业务事务
    /// </summary>
    internal class BizTranscation : ITransactable, IBizTranscation
    {
        /// <summary>
        /// 创建一个业务事务
        /// </summary>
        /// <param name="bizEntity">可建立事务的对象</param>
        internal BizTranscation(ITransactable bizEntity)
        {
            this.transcationList = new List<ITransactable>() { bizEntity };
        }
        private List<ITransactable> transcationList;
        /// <summary>
        /// 提交事务
        /// </summary>
        public void Commit()
        {
            foreach (var transcation in this.transcationList)
            {
                transcation.Commit();
            }
        }
        /// <summary>
        /// 回滚事务
        /// </summary>
        public void Rollback()
        {
            foreach (var transcation in this.transcationList)
            {
                transcation.Rollback();
            }
        }
    }
}
