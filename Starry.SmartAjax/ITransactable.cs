﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Starry.SmartAjax
{
    /// <summary>
    /// 可进行事务操作的接口
    /// </summary>
    public interface ITransactable
    {
        /// <summary>
        /// 提交事务
        /// </summary>
        void Commit();
        /// <summary>
        /// 回滚事务
        /// </summary>
        void Rollback();
    }
}