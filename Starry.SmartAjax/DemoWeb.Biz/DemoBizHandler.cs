﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoWeb.Biz
{
    class DemoBizHandler
        : Starry.SmartAjax.BizHandler
    {
        protected override Starry.SmartAjax.IBizFactory BizFactory
        {
            get { return new DemoBizFactory(); }
        }
    }
}
